package com.springapp.mvc.entities;

import javax.persistence.*;

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 29.07.2014
 */
@Entity
@Table(name = "CHILD")
public class Child {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "FIELD")
    private Integer field;

    @ManyToOne
    @JoinColumn(name = "PARENT_ID", nullable = false)
    private Parent parent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
}
