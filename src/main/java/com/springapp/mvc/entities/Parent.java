package com.springapp.mvc.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 29.07.2014
 */
@Entity
@Table(name = "PARENT")
public class Parent {
    public Date date = new Date();

    private Integer id;

    private Integer field;

    private Set<Child> children = new HashSet<Child>();

    @Id
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FIELD")
    public Integer getField() {
        return field;
    }

    public void setField(Integer field) {
        this.field = field;
    }

    @OneToMany(mappedBy = "parent")
    public Set<Child> getChildren() {
        return children;
    }

    public void setChildren(Set<Child> children) {
        this.children = children;
    }
}
