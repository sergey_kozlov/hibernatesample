package com.springapp.mvc.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.springapp.mvc.entities.Child;
import org.springframework.stereotype.Repository;

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 29.07.2014
 */
@Repository
public class ChildDao extends GenericDAOImpl<Child, Integer> {
}
