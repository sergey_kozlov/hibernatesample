package com.springapp.mvc.dao;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;
import com.googlecode.genericdao.search.hibernate.HibernateMetadataUtil;
import com.springapp.mvc.entities.Parent;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 29.07.2014
 */
@Repository
public class ParentDao extends GenericDAOImpl<Parent, Integer> {
}
