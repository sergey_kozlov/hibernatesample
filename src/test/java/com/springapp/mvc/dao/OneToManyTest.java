package com.springapp.mvc.dao;

import com.springapp.mvc.entities.Child;
import com.springapp.mvc.entities.Parent;
import static junit.framework.Assert.*;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 29.07.2014
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/persistence.xml")
public class OneToManyTest {
    @Autowired
    private ParentDao parents;

    @Autowired
    private ChildDao children;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Rollback(false)
    public void insert(){
        Session session = sessionFactory.getCurrentSession();
        Parent parent = new Parent();
        parent.setId(0);
        parent.setField(1);
        session.save(parent);
        for (int i = 0; i < 2; ++i){
            Child child = new Child();
            child.setId(i);
            child.setField(0);
            child.setParent(parent);
            session.save(child);
        }
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Rollback(false)
    public void change(){
        Parent parent = parents.find(0);
        assertEquals(1, (int)parent.getField());
        parent.setField(2);
        Set<Child> children = parent.getChildren();
        assertEquals(2, children.size());
        for (Child child : children){
            child.setField(1);
        }
        Child newChild = new Child();
        newChild.setId(3);
        newChild.setField(8);
        newChild.setParent(parent);
        children.add(newChild);
        parents.save(parent);
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Rollback(false)
    public void checkAndClean(){
        Parent parent = parents.find(0);
        assertEquals(2, (int)parent.getField());
        Set<Child> children = parent.getChildren();
        assertEquals(2, children.size());
        for (Child child : children){
            assertEquals(1, (int)child.getField());
        }
        clean(parent);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Rollback(false)
    public void clean(Parent parent){
        Set<Child> children = parent.getChildren();
        if(children != null) {
            for (Child child : children) {
                this.children.remove(child);
            }
        }
        parents.remove(parent);
    }
}
